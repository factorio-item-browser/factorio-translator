# Factorio Translator

The Factorio Translator package implements the translator of Factorio in Go. Given the locale files shipped with the 
mods it is able to translate the localised strings used by Factorio into any locale provided by the mods.

[[_TOC_]]

## Features

- Translate any localised string to any locale, providing the locale files of mods.
- Load mods either in their archive format (e.g. downloaded from the Factorio mod portal), or an already extracted
  directory (as are the core and base mods from Factorio itself).
- Resolve positional parameters `__1__` from the localised strings.
- Resolve simple references like `__ITEM__electronic-circuit__` and `__ENTITY__iron-ore__` by replacing them with their
  translated name.
- Resolve plural forms like `__plural_for_parameter_1_{1=hour|rest=hours}__`.
- Process control references like `__CONTROL__build__` and `__ALT_CONTROL__1__build__` by providing an abstract class
  for implementation.
- Process RichText tags like `[item=electronic-circuit]` and `[color=red]text[/color]` by providing abstract classes for
  implementation.

## Installation

```bash
go get gitlab.com/factorio-item-browser/factorio-translator
```

## Usage

The translator requires some setup steps before it can actually be used. Otherwise, it won't do much at all.

### Create instance

A translator instance is created using the `New()` function. The function expects any of the option functions to be 
passed in, to configure the translator. Configuration is required for loading mods, and for processing special parts
of the translated messages. See below for more information regarding the loaders and processors.

The following example shows how to create an instance of the translator without any special processing:

```go
import "gitlab.com/factorio-item-browser/factorio-translator.git/pkg/translator"

t := translator.New(
	translator.WithDefaultLoaders(),
	translator.WithCommonProcessors(),
)
```

After the instance is created, you have to provide the paths to the mods to be loaded into the translator:

```go
_ = ft.LoadMod("/path/to/factorio/data/core")
_ = ft.LoadMod("/path/to/factorio/data/base")
_ = ft.LoadMod("/path/to/factorio/mods/my-fancy-mod_1.33.7.zip")
```

Some notes on loading mods:

- The `core` mod from Factorio should always be loaded first, and the `base` mod should always be loaded second. Not
  doing so may lead to missing translations.
- The order of the mods actually matters: If two mods provide a translation for the same key, the later one added will
  win. This is the same is Factorio would do it. Ideally all mods get added in the same order as Factorio loads them.

Now the translator is ready to be used for actually translating localised strings:

```go
// Prints "Electronic circuit"
fmt.Println(t.Translate("en", []any{"item-name.electronic-circuit"}))                          
// Prints "Elektronischer Schaltkreis"
fmt.Println(t.Translate("de", []any{"item-name.electronic-circuit"}))
// Prints "Fill Crude oil barrel"
fmt.Println(t.Translate("en", []any{"recipe-name.fill-barrel", []any{"fluid-name.crude-oil"}})) 
```

The first parameter is the locale to translate the localised string into. The values are the same as used by Factorio.
A list of all locales can be obtained by calling `t.Storage().Locales()`.

If a translation is not available, `Translate()` will return an empty string. If you want to fall back to English
instead, use `TranslateWithFallback()` instead.

Note that the localised strings must be specified in Go syntax, i.e. the lua tables must be transformed to Go slices.
The translator does not understand the lua syntax.

### Loaders

To be able to actually add mods (or especially their locale files) to the translator, mod loaders must be added first.

The package ships with two loaders, covering the most common use cases:

- **loader.ModArchive:** Loads the mods from the zipped archive. This loader can be used to load any mods downloaded
  from the Factorio mod portal.
- **loader.ModDirectory:** Loads mods from a directory. This loader can be used to load the `core` and `base` mods from
  the Factorio game itself, as they are shipped uncompressed with the game.

To add a loader to the translator, pass it using the `WithLoader()` function as option to the constructor. This way you
can add any loader to need to actually load the locale files. In case you only need the basic use cases of using 
unzipped or zipped mods, you can use `WithDefaultLoaders()` instead, which will add both of the mentioned loaders.

### Processors

The translator uses text processors to process translated strings further. An example of such processing is to replace
special references in the string with their actual values. While the translator ships with some basic processors, it may
be required to implement your own processors to get all the features out of the translators.

A processor can be added to the translator by using the `WithProcessor()` function as option to the constructor. 

#### Common processors

The translator comes with some common processors. It is recommended to always add these to the translator, as they
handle very basic features of localised strings.

- **PositionPlaceholderProcessor:** Handles position references for parameters like `__1__`.
- **EntityPlaceholderProcessor:** Handles entity references like `__ENTITY__iron-ore__`, replacing it with the
  translated name of the entity.
- **ItemPlaceholderProcessor:** Handles item references like `__ITEM__electronic-circuit__`, replacing it with the
  translated name of the item.
- **PluralPlaceholderProcessor:** Handles the special plural form syntax of Factorio, like
  `__plural_for_parameter_1_{1=hour|rest=hours}__`.

To add these common processors, simply use the `WithCommonProcessors()` function. This will add all of the mentioned
processors to the translator.

Add these processors as following:

#### Advanced processors

The translator also provides some processor creators, which require an additional callback to actually do its job.
These processor creators include:

- **NewControlPlaceholderProcessor():** Handles placeholders like `__CONTROL__build__` and
  `__ALT_CONTROL__1__build__`. The callback must provide the replacement of the control.
- **NewStandaloneTagProcessor():** Handles simple RichText tags without content, e.g. `[item=electronic-circuit]`.
  The creator handles parsing of the tags (but not of the tag value), and the callback must provide the replacement for
  the tag.
- **NewContentTagProcessor():** Handles complex RichText tags, having content between them, like `[color=red]foo[/color]`
  and `[font=bold]foo[/font]`. The creator already handles parsing the string for the tags, and the callback must
  provide the replacement for them, e.g. HTML format.

The following example shows how to implement and use the control placeholder processor:

```go
WithProcessor(
    processor.NewControlPlaceholderProcessor(func(translator processor.Translator, locale string, controlName string, version int) (string, bool) {
        // Use the translated name of the control (as it appears in the options menu),
        // and put it into square brackets.
        control := translator.TranslateWithFallback(locale, []any{fmt.Sprintf("controls.%s", controlName)})
        return fmt.Sprintf("[%s]", control), true
    }),
)
```

## Further reading

Further information and documentations on how the localised strings and the translation system works in Factorio can be
found on the following websites:

- Factorio API: [LocalisedString](https://lua-api.factorio.com/latest/Concepts.html#LocalisedString)
- Factorio wiki: [Tutorial:Localisation](https://wiki.factorio.com/Tutorial:Localisation)
- Factorio wiki: [Rich text](https://wiki.factorio.com/Rich_text)