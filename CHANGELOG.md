# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v1.0.0] - 2023-01-22

### Added

- Initial release of the package, ported from 
  [bluepsyduck/factorio-translator](https://github.com/BluePsyduck/factorio-translator) for PHP.

[Unreleased]: https://gitlab.com/factorio-item-browser/factorio-translator/-/compare/v1.0.0...HEAD
[v1.0.0]: https://gitlab.com/factorio-item-browser/factorio-translator/-/tags/v1.0.0