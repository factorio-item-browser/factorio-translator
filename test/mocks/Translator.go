// Code generated by mockery v2.14.0. DO NOT EDIT.

package mocks

import mock "github.com/stretchr/testify/mock"

// Translator is an autogenerated mock type for the Translator type
type Translator struct {
	mock.Mock
}

// ApplyProcessors provides a mock function with given fields: locale, value, parameters
func (_m *Translator) ApplyProcessors(locale string, value string, parameters []interface{}) string {
	ret := _m.Called(locale, value, parameters)

	var r0 string
	if rf, ok := ret.Get(0).(func(string, string, []interface{}) string); ok {
		r0 = rf(locale, value, parameters)
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// Translate provides a mock function with given fields: locale, localisedString
func (_m *Translator) Translate(locale string, localisedString interface{}) string {
	ret := _m.Called(locale, localisedString)

	var r0 string
	if rf, ok := ret.Get(0).(func(string, interface{}) string); ok {
		r0 = rf(locale, localisedString)
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

// TranslateWithFallback provides a mock function with given fields: locale, localisedString
func (_m *Translator) TranslateWithFallback(locale string, localisedString interface{}) string {
	ret := _m.Called(locale, localisedString)

	var r0 string
	if rf, ok := ret.Get(0).(func(string, interface{}) string); ok {
		r0 = rf(locale, localisedString)
	} else {
		r0 = ret.Get(0).(string)
	}

	return r0
}

type mockConstructorTestingTNewTranslator interface {
	mock.TestingT
	Cleanup(func())
}

// NewTranslator creates a new instance of Translator. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewTranslator(t mockConstructorTestingTNewTranslator) *Translator {
	mock := &Translator{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
