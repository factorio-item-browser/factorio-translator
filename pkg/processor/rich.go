package processor

import (
	"fmt"
	"golang.org/x/exp/slices"
	"regexp"
)

type (
	StandaloneTagProcessor = func(translator Translator, locale, name, value string) (string, bool)
	ContentTagProcessor    = func(translator Translator, locale, name, value, content string) (string, bool)
)

var (
	regexpStandaloneTag = regexp.MustCompile(`(?U)\[(.+)=(.+)]`)
	regexpContentTag    = regexp.MustCompile(`(?U)\[((color|font)=(.+)|([./])(color|font))]`)

	standaloneTagNames = []string{
		"img",
		"item",
		"entity",
		"technology",
		"recipe",
		"item-group",
		"fluid",
		"tile",
		"virtual-signal",
		"achievement",
		"gps",
		"special-item",
		"armor",
		"train",
		"train-stop",
	}
)

// NewStandaloneTagProcessor creates a new processor for replacing standalone tags like [item=electronic-circuit]. Each
// found tag will be passed to the tagProcessor, which must provide the replacement for the tag, and whether the
// processing was actually successful.
func NewStandaloneTagProcessor(tagProcessor StandaloneTagProcessor) Processor {
	return NewRegexpProcessor(regexpStandaloneTag, func(translator Translator, locale string, values []string, _ []any) (string, bool) {
		name := values[0]
		value := values[1]
		if !slices.Contains(standaloneTagNames, name) {
			return "", false
		}

		return tagProcessor(translator, locale, name, value)
	})
}

// NewContentTagProcessor creates a new processor for replacing pairs of tags with actual content between them, like
// [color=red]foo[/color]. The only possible tags are [color] and [font], all other tags will be ignored. Both ending
// tags [/color] and [.color] will be detected. Closing tags must match the opening counterparts, mismatched tags will
// remain untouched.
// Each tag will be passed to the tagProcessor, including the already-processed content between it. The tagProcessor
// must return the replacement for the tag, and whether processing it was actually successful.
func NewContentTagProcessor(tagProcessor ContentTagProcessor) Processor {
	return func(translator Translator, locale string, value string, parameters []any) string {
		position := 0
		contents := []string{""}
		openedTags := make([]string, 0)

		matches := regexpContentTag.FindAllStringSubmatchIndex(value, -1)

		for _, match := range matches {
			// Copy everything up to the tag and set the position to after it.
			contents[0] = contents[0] + value[position:match[0]]
			position = match[1]

			if match[8] != -1 {
				// Closing tag
				closingChar := value[match[8]:match[9]]
				tagName := value[match[10]:match[11]]

				if len(openedTags) < 2 || openedTags[0] != tagName {
					// Tag mismatch, ignore closing tag, copy as-is.
					contents[0] = contents[0] + value[match[0]:match[1]]
					continue
				}

				openedTagName := openedTags[0]
				openedTagValue := openedTags[1]
				openedTags = openedTags[2:]

				content := contents[0]
				contents = contents[1:]

				replacement, ok := tagProcessor(translator, locale, openedTagName, openedTagValue, content)
				if !ok {
					replacement = fmt.Sprintf("[%s=%s]%s[%s%s]", openedTagName, openedTagValue, content, closingChar, tagName)
				}
				contents[0] = contents[0] + replacement
			} else {
				// Opening tag
				tagName := value[match[4]:match[5]]
				tagValue := value[match[6]:match[7]]

				openedTags = append([]string{tagName, tagValue}, openedTags...)
				contents = append([]string{""}, contents...)
			}
		}

		// Porcess oll opened tags which have not been closed.
		for i := 0; i < len(openedTags); i = i + 2 {
			tagName := openedTags[i]
			tagValue := openedTags[i+1]

			content := contents[0]
			contents = contents[1:]

			contents[0] = contents[0] + fmt.Sprintf("[%s=%s]%s", tagName, tagValue, content)
		}

		// Copy the end of the string after the last tag.
		contents[0] = contents[0] + value[position:]
		return contents[0]
	}
}
