package processor

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/factorio-translator.git/test/mocks"
	"testing"
)

func TestNewStandaloneTagProcessor(t *testing.T) {
	tests := map[string]struct {
		tagProcessor   StandaloneTagProcessor
		value          string
		expectedResult string
	}{
		"known tag": {
			tagProcessor: func(_ Translator, _, name, value string) (string, bool) {
				return fmt.Sprintf("{%s=%s}", name, value), true
			},
			value:          "abc [item=def] ghi",
			expectedResult: "abc {item=def} ghi",
		},
		"unknown tag": {
			tagProcessor: func(_ Translator, _, name, value string) (string, bool) {
				return fmt.Sprintf("{%s=%s}", name, value), true
			},
			value:          "abc [unknown=def] ghi",
			expectedResult: "abc [unknown=def] ghi",
		},
		"processor error": {
			tagProcessor: func(_ Translator, _, _, _ string) (string, bool) {
				return "", false
			},
			value:          "abc [item=def] ghi",
			expectedResult: "abc [item=def] ghi",
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			translator := mocks.NewTranslator(t)

			instance := NewStandaloneTagProcessor(test.tagProcessor)
			result := instance(translator, "foo", test.value, []any{"bar"})

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestNewContentTagProcessor(t *testing.T) {
	tests := map[string]struct {
		tagProcessor   ContentTagProcessor
		value          string
		expectedResult string
	}{
		"happy path": {
			tagProcessor: func(_ Translator, _, name, value, content string) (string, bool) {
				return fmt.Sprintf("{%s=%v|%s}", name, value, content), true
			},
			value:          "abc [color=red] def [font=bold] ghi [.font] jkl [/color] mno",
			expectedResult: "abc {color=red| def {font=bold| ghi } jkl } mno",
		},
		"mismatched tags": {
			tagProcessor: func(_ Translator, _, name, value, content string) (string, bool) {
				return fmt.Sprintf("{%s=%s|%s}", name, value, content), true
			},
			value:          "abc [color=red] def [font=bold] ghi [.color] jkl [/font] mno",
			expectedResult: "abc [color=red] def {font=bold| ghi [.color] jkl } mno",
		},
		"processor error": {
			tagProcessor: func(_ Translator, _, _, _, _ string) (string, bool) {
				return "", false
			},
			value:          "abc [color=red] def [font=bold] ghi [.font] jkl [/color] mno",
			expectedResult: "abc [color=red] def [font=bold] ghi [.font] jkl [/color] mno",
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			translator := mocks.NewTranslator(t)

			instance := NewContentTagProcessor(test.tagProcessor)
			result := instance(translator, "foo", test.value, []any{"bar"})

			assert.Equal(t, test.expectedResult, result)
		})
	}
}
