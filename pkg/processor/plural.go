package processor

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var (
	regexpPluralPlaceholder = regexp.MustCompile(`(?U)__plural_for_parameter_(\d+)_\{(.+)}__`)
	regexpPluralEndsIn      = regexp.MustCompile(`^ends in (\d+)$`)

	// PluralPlaceholder replaces the plural placeholders like __plural_for_parameter_1_{...}__.
	PluralPlaceholder = NewRegexpProcessor(regexpPluralPlaceholder, processPluralPlaceholder)
)

func processPluralPlaceholder(translator Translator, locale string, values []string, parameters []any) (string, bool) {
	position, _ := strconv.Atoi(values[0])
	if len(parameters) < position {
		return "", false
	}

	parameter, err := strconv.Atoi(fmt.Sprintf("%v", parameters[position-1]))
	if err != nil {
		return "", false
	}

	return processPluralConditions(translator, locale, values[1], parameter)
}

func processPluralConditions(translator Translator, locale string, conditionString string, number int) (string, bool) {
	for _, condition := range strings.Split(conditionString, "|") {
		parts := strings.SplitN(condition, "=", 2)
		if len(parts) != 2 {
			continue
		}

		for _, c := range strings.Split(parts[0], ",") {
			if !evaluatePluralCondition(c, number) {
				continue
			}

			result := translator.ApplyProcessors(locale, parts[1], []any{})
			return result, true
		}
	}

	return "", false
}

func evaluatePluralCondition(condition string, number int) bool {
	// "rest" matches everything, representing the else case.
	if condition == "rest" {
		return true
	}

	// Numeric values must be matched exactly.
	if n, err := strconv.Atoi(condition); err == nil {
		return n == number
	}

	// "ends in" defines a suffix to match.
	if match := regexpPluralEndsIn.FindStringSubmatch(condition); match != nil {
		return strings.HasSuffix(fmt.Sprintf("%d", number), match[1])
	}

	return false
}
