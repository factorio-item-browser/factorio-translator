package processor

type Translator interface {
	ApplyProcessors(locale string, value string, parameters []any) string
	Translate(locale string, localisedString any) string
	TranslateWithFallback(locale string, localisedString any) string
}

// Processor is the type of functions able to process already translated strings, e.g. by replacing placeholders in
// them. The function returns the transformed value.
type Processor = func(translator Translator, locale string, value string, parameters []any) string
