package processor

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/factorio-translator.git/test/mocks"
	"testing"
)

func TestPluralPlaceholder(t *testing.T) {
	type translatorCall struct {
		value  any
		result string
	}

	tests := map[string]struct {
		value           string
		parameters      []any
		translatorCalls []translatorCall
		expectedResult  string
	}{
		"number parameter": {
			value:      "abc __plural_for_parameter_2_{42=def|rest=ghi}__ jkl",
			parameters: []any{21, 42},
			translatorCalls: []translatorCall{
				{value: "def", result: "fed"},
			},
			expectedResult: "abc fed jkl",
		},
		"rest case": {
			value:      "abc __plural_for_parameter_2_{42=def|rest=ghi}__ jkl",
			parameters: []any{21, 1337},
			translatorCalls: []translatorCall{
				{value: "ghi", result: "ihg"},
			},
			expectedResult: "abc ihg jkl",
		},
		"string parameter": {
			value:      "abc __plural_for_parameter_2_{42=def|rest=ghi}__ jkl",
			parameters: []any{"21", "42"},
			translatorCalls: []translatorCall{
				{value: "def", result: "fed"},
			},
			expectedResult: "abc fed jkl",
		},
		"non-number parameter": {
			value:           "abc __plural_for_parameter_2_{42=def|rest=ghi}__ jkl",
			parameters:      []any{"21", "abc"},
			translatorCalls: []translatorCall{},
			expectedResult:  "abc __plural_for_parameter_2_{42=def|rest=ghi}__ jkl",
		},
		"no match": {
			value:           "abc __plural_for_parameter_2_{42=def}__ jkl",
			parameters:      []any{21, 1337},
			translatorCalls: []translatorCall{},
			expectedResult:  "abc __plural_for_parameter_2_{42=def}__ jkl",
		},
		"missing parameter": {
			value:           "abc __plural_for_parameter_2_{42=def|rest=ghi}__ jkl",
			parameters:      []any{21},
			translatorCalls: []translatorCall{},
			expectedResult:  "abc __plural_for_parameter_2_{42=def|rest=ghi}__ jkl",
		},
		"malformed conditions": {
			value:           "abc __plural_for_parameter_2_{invalid}__ jkl",
			parameters:      []any{21, 42},
			translatorCalls: []translatorCall{},
			expectedResult:  "abc __plural_for_parameter_2_{invalid}__ jkl",
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			locale := "foo"

			translator := mocks.NewTranslator(t)
			for _, call := range test.translatorCalls {
				translator.On("ApplyProcessors", locale, call.value, []any{}).Once().Return(call.result)
			}

			result := PluralPlaceholder(translator, locale, test.value, test.parameters)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestEvaluatePluralCondition(t *testing.T) {
	tests := map[string]struct {
		condition      string
		number         int
		expectedResult bool
	}{
		"rest": {
			condition:      "rest",
			number:         42,
			expectedResult: true,
		},
		"42 == 42": {
			condition:      "42",
			number:         42,
			expectedResult: true,
		},
		"1337 != 42": {
			condition:      "1337",
			number:         42,
			expectedResult: false,
		},
		"2 != 42": {
			condition:      "2",
			number:         42,
			expectedResult: false,
		},
		"42 != 2": {
			condition:      "42",
			number:         2,
			expectedResult: false,
		},
		"ends in 2 == 42": {
			condition:      "ends in 2",
			number:         42,
			expectedResult: true,
		},
		"ends in 2 != 21": {
			condition:      "ends in 2",
			number:         21,
			expectedResult: false,
		},
		"ends in 42 == 42": {
			condition:      "ends in 42",
			number:         42,
			expectedResult: true,
		},
		"ends in 42 != 2": {
			condition:      "42",
			number:         2,
			expectedResult: false,
		},
		"invalid": {
			condition:      "invalid",
			number:         42,
			expectedResult: false,
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			result := evaluatePluralCondition(test.condition, test.number)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}
