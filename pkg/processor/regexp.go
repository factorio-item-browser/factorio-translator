package processor

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var (
	regexpControlPlaceholder  = regexp.MustCompile(`(?U)__(CONTROL|ALT_CONTROL__(\d+))__(.+)__`)
	regexpEntityPlaceholder   = regexp.MustCompile(`(?U)__ENTITY__(.+)__`)
	regexpItemPlaceholder     = regexp.MustCompile(`(?U)__ITEM__(.+)__`)
	regexpPositionPlaceholder = regexp.MustCompile(`(?U)__(\d+)__`)

	// EntityPlaceholder replaces entity references like __ENTITY__coal with their translation.
	EntityPlaceholder = NewRegexpProcessor(regexpEntityPlaceholder, processEntityPlaceholder)
	// ItemPlaceholder replaces item references like __ITEM__electronic-circuit__ with their translation.
	ItemPlaceholder = NewRegexpProcessor(regexpItemPlaceholder, processItemPlaceholder)
	// PositionPlaceholder replaces positional placeholders like __1__ with the translated parameter.
	PositionPlaceholder = NewRegexpProcessor(regexpPositionPlaceholder, processPositionPlaceholder)
)

type (
	ControlProcessor = func(translator Translator, locale string, controlName string, version int) (string, bool)
	MatchProcessor   = func(translator Translator, locale string, values []string, parameters []any) (string, bool)
)

// NewRegexpProcessor creates a new processor based on a regular expression. The regular expression is run on the input,
// and each match to the expression is passed to the matchProcessor. Each matched groups of the expression get passed in
// as values to the matchProcessor. The matchProcessor must return the replacement, and whether the processing was
// actually successful.
func NewRegexpProcessor(regexp *regexp.Regexp, matchProcessor MatchProcessor) Processor {
	return func(translator Translator, locale string, value string, parameters []any) string {
		placeholders := findPlaceholders(regexp, value)
		for placeholder, values := range placeholders {
			if replacement, ok := matchProcessor(translator, locale, values, parameters); ok {
				value = strings.ReplaceAll(value, placeholder, replacement)
			}
		}
		return value
	}
}

func findPlaceholders(regexp *regexp.Regexp, value string) map[string][]string {
	matches := regexp.FindAllStringSubmatch(value, -1)

	result := make(map[string][]string, len(matches))
	for _, match := range matches {
		result[match[0]] = match[1:]
	}
	return result
}

func processEntityPlaceholder(translator Translator, locale string, values []string, _ []any) (string, bool) {
	result := translator.TranslateWithFallback(locale, []any{fmt.Sprintf("entity-name.%s", values[0])})
	return result, true
}

func processItemPlaceholder(translator Translator, locale string, values []string, _ []any) (string, bool) {
	result := translator.TranslateWithFallback(locale, []any{fmt.Sprintf("item-name.%s", values[0])})
	return result, true
}

func processPositionPlaceholder(translator Translator, locale string, values []string, parameters []any) (string, bool) {
	position, _ := strconv.Atoi(values[0])
	if len(parameters) < position {
		return "", false
	}

	result := translator.TranslateWithFallback(locale, parameters[position-1])
	return result, true
}

// NewControlPlaceholderProcessor creates a new processor which replaces references to controls like __CONTROL__build__
// as well as to alternative controls like __ALT_CONTROL__2__build__. Whenever a control placeholder is detected, the
// provided controlProcessor will be called. In case of normal controls the version will always be 0, in case of
// alternative controls the number from the placeholder will be passed in as version. The controlProcessor must return
// the replacement value, and whether the processing was actually successful.
func NewControlPlaceholderProcessor(controlProcessor ControlProcessor) Processor {
	return NewRegexpProcessor(regexpControlPlaceholder, func(translator Translator, locale string, values []string, _ []any) (string, bool) {
		controlName := values[2]
		version, _ := strconv.Atoi(values[1])

		return controlProcessor(translator, locale, controlName, version)
	})
}
