package processor

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/factorio-translator.git/test/mocks"
	"testing"
)

func TestEntityPlaceholder(t *testing.T) {
	locale := "foo"
	value := "abc __ENTITY__def__ ghi"
	parameters := make([]any, 0)
	expectedLocalisedString := []any{"entity-name.def"}
	resultTranslator := "fed"
	expectedResult := "abc fed ghi"

	translator := mocks.NewTranslator(t)
	translator.On("TranslateWithFallback", locale, expectedLocalisedString).Once().Return(resultTranslator)

	result := EntityPlaceholder(translator, locale, value, parameters)

	assert.Equal(t, expectedResult, result)
}

func TestItemPlaceholder(t *testing.T) {
	locale := "foo"
	value := "abc __ITEM__def__ ghi"
	parameters := make([]any, 0)
	expectedLocalisedString := []any{"item-name.def"}
	resultTranslator := "fed"
	expectedResult := "abc fed ghi"

	translator := mocks.NewTranslator(t)
	translator.On("TranslateWithFallback", locale, expectedLocalisedString).Once().Return(resultTranslator)

	result := ItemPlaceholder(translator, locale, value, parameters)

	assert.Equal(t, expectedResult, result)
}

func TestPositionPlaceholder(t *testing.T) {
	type translatorCall struct {
		locale string
		value  any
		result string
	}

	tests := map[string]struct {
		locale          string
		value           string
		parameters      []any
		translatorCalls []translatorCall
		expectedResult  string
	}{
		"single position": {
			locale:     "foo",
			value:      "abc __2__ def",
			parameters: []any{"oof", "bar", "baz"},
			translatorCalls: []translatorCall{
				{locale: "foo", value: "bar", result: "rab"},
			},
			expectedResult: "abc rab def",
		},
		"two positions": {
			locale:     "foo",
			value:      "abc __2__ def __1__ ghi",
			parameters: []any{"oof", "bar"},
			translatorCalls: []translatorCall{
				{locale: "foo", value: "oof", result: "baz"},
				{locale: "foo", value: "bar", result: "rab"},
			},
			expectedResult: "abc rab def baz ghi",
		},
		"missing parameter": {
			locale:          "foo",
			value:           "abc __2__ def",
			parameters:      []any{"oof"},
			translatorCalls: []translatorCall{},
			expectedResult:  "abc __2__ def",
		},
		"number parameter": {
			locale:     "foo",
			value:      "abc __2__ def",
			parameters: []any{"oof", 42, "baz"},
			translatorCalls: []translatorCall{
				{locale: "foo", value: 42, result: "21"},
			},
			expectedResult: "abc 21 def",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			translator := mocks.NewTranslator(t)
			for _, call := range test.translatorCalls {
				translator.On("TranslateWithFallback", call.locale, call.value).Once().Return(call.result)
			}

			result := PositionPlaceholder(translator, "foo", test.value, test.parameters)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestNewControlPlaceholderProcessor(t *testing.T) {
	tests := map[string]struct {
		controlProcessor ControlProcessor
		value            string
		expectedResult   string
	}{
		"control": {
			controlProcessor: func(_ Translator, _ string, controlName string, version int) (string, bool) {
				return fmt.Sprintf("%s#%d", controlName, version), true
			},
			value:          "abc __CONTROL__build__ def",
			expectedResult: "abc build#0 def",
		},
		"alt-control": {
			controlProcessor: func(_ Translator, _ string, controlName string, version int) (string, bool) {
				return fmt.Sprintf("%s#%d", controlName, version), true
			},
			value:          "abc __ALT_CONTROL__42__build__ def",
			expectedResult: "abc build#42 def",
		},
		"processor error": {
			controlProcessor: func(_ Translator, _ string, _ string, _ int) (string, bool) {
				return "", false
			},
			value:          "abc __CONTROL__build__ def",
			expectedResult: "abc __CONTROL__build__ def",
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			locale := "foo"
			parameters := make([]any, 0)

			translator := mocks.NewTranslator(t)

			instance := NewControlPlaceholderProcessor(test.controlProcessor)
			result := instance(translator, locale, test.value, parameters)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}
