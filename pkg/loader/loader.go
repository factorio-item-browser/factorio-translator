package loader

import (
	"archive/zip"
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

type Storage interface {
	Set(locale, section, name, value string)
}

// Loader is the type of functions able to load translation files. The function returns whether the provided source
// was successfully processed.
type Loader = func(storage Storage, source string) bool

var regexpFile = regexp.MustCompile(`^[^/]+/locale/([^/]+)/[^/]+\.cfg$`)

// ModDirectory loads the locale files of an already extracted mod. source is the mod directory, containing the
// info.json file.
func ModDirectory(storage Storage, source string) bool {
	fileInfo, err := os.Stat(source)
	if err != nil || !fileInfo.IsDir() {
		return false
	}

	files, _ := filepath.Glob(fmt.Sprintf("%s/locale/*/*.cfg", source))
	for _, file := range files {
		parts := strings.Split(file, "/")
		locale := parts[len(parts)-2]

		if fp, err := os.Open(file); err == nil {
			processFile(fp, storage, locale)
			_ = fp.Close()
		}
	}
	return true
}

// ModArchive loads the locale files of a zipped mod. source is the path to the zip archive file.
func ModArchive(storage Storage, source string) bool {
	zipArchive, err := zip.OpenReader(source)
	if err != nil {
		return false
	}

	for _, file := range zipArchive.File {
		if file.FileInfo().IsDir() {
			continue
		}

		match := regexpFile.FindStringSubmatch(file.FileHeader.Name)
		if match == nil {
			continue
		}
		locale := match[1]

		if fp, err := file.Open(); err == nil {
			processFile(fp, storage, locale)
			_ = fp.Close()
		}
	}
	return true
}

func processFile(reader io.Reader, storage Storage, locale string) {
	section := ""

	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := strings.Trim(scanner.Text(), "\r")
		trimmedLine := strings.Trim(line, " \n\r\t\v\x00")

		if strings.HasPrefix(trimmedLine, ";") {
			// Skip comment lines
			continue
		}

		parts := strings.SplitN(line, "=", 2)
		if len(parts) == 2 {
			// We have a key-value pair
			name := strings.Trim(parts[0], " \n\r\t\v\x00")
			value := strings.ReplaceAll(parts[1], "\\n", "\n") // Make linebreaks to actual linebreaks.

			storage.Set(locale, section, name, value)
			continue
		}

		if strings.HasPrefix(trimmedLine, "[") && strings.HasSuffix(trimmedLine, "]") {
			section = trimmedLine[1 : len(trimmedLine)-1]
			continue
		}
	}
}
