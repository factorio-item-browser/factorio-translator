package loader

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/factorio-translator.git/test/mocks"
	"path/filepath"
	"runtime"
	"strings"
	"testing"
)

func TestModArchive(t *testing.T) {
	type storageCall struct {
		locale  string
		section string
		name    string
		value   string
	}

	tests := map[string]struct {
		source         string
		storageCalls   []storageCall
		expectedResult bool
	}{
		"mod-archive.zip": {
			source: "mod-archive.zip",
			storageCalls: []storageCall{
				{locale: "foo", section: "", name: "foo", value: "bar"},
			},
			expectedResult: true,
		},
		"invalid-archive.zip": {
			source:         "invalid-archive.zip",
			storageCalls:   []storageCall{},
			expectedResult: false,
		},
		"mod-directory": {
			source:         "mod-directory",
			storageCalls:   []storageCall{},
			expectedResult: false,
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			storage := mocks.NewStorage(t)
			for _, call := range test.storageCalls {
				storage.On("Set", call.locale, call.section, call.name, call.value).Once()
			}

			_, filename, _, _ := runtime.Caller(0)
			source := fmt.Sprintf("%s/../../test/assets/loader/%s", filepath.Dir(filename), test.source)

			result := ModArchive(storage, source)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestModDirectory(t *testing.T) {
	type storageCall struct {
		locale  string
		section string
		name    string
		value   string
	}

	tests := map[string]struct {
		source         string
		storageCalls   []storageCall
		expectedResult bool
	}{
		"mod-directory": {
			source: "mod-directory",
			storageCalls: []storageCall{
				{locale: "foo", section: "", name: "foo", value: "bar"},
			},
			expectedResult: true,
		},
		"mod-archive.zip": {
			source:         "mod-archive.zip",
			storageCalls:   []storageCall{},
			expectedResult: false,
		},
		"invalid-archive.zip": {
			source:         "invalid-archive.zip",
			storageCalls:   []storageCall{},
			expectedResult: false,
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			storage := mocks.NewStorage(t)
			for _, call := range test.storageCalls {
				storage.On("Set", call.locale, call.section, call.name, call.value).Once()
			}

			_, filename, _, _ := runtime.Caller(0)
			source := fmt.Sprintf("%s/../../test/assets/loader/%s", filepath.Dir(filename), test.source)

			result := ModDirectory(storage, source)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestProcessFile(t *testing.T) {
	locale := "foo"
	contents := `
abc=def
	ghi=jkl
;fail=comment
[mno]
abc=pqr 
`

	storage := mocks.NewStorage(t)
	storage.On("Set", locale, "", "abc", "def").Once()
	storage.On("Set", locale, "", "ghi", "jkl").Once()
	storage.On("Set", locale, "mno", "abc", "pqr ").Once()

	processFile(strings.NewReader(contents), storage, locale)
}
