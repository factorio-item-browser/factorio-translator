package translator

import (
	"fmt"
	"gitlab.com/factorio-item-browser/factorio-translator.git/pkg/loader"
	"gitlab.com/factorio-item-browser/factorio-translator.git/pkg/processor"
	"reflect"
	"strings"
)

const fallbackLocale = "en"

type Option func(*Translator)

type Translator struct {
	loaders    []loader.Loader
	processors []processor.Processor
	storage    *Storage
}

func New(options ...Option) *Translator {
	instance := Translator{
		loaders:    make([]loader.Loader, 0),
		processors: make([]processor.Processor, 0),
		storage:    NewStorage(),
	}

	for _, option := range options {
		option(&instance)
	}
	return &instance
}

// WithLoader will add the provided loader to the translator instance. The translator will go through all loaders in
// the same order as they were added, until the first one was able to actually load the mod.
func WithLoader(l loader.Loader) Option {
	return func(t *Translator) {
		t.loaders = append(t.loaders, l)
	}
}

// WithDefaultLoaders will load the default loaders to the translator instance. In most cases these should suffice to
// load the locale files from zipped and unzipped mods.
func WithDefaultLoaders() Option {
	return func(t *Translator) {
		t.loaders = append(t.loaders, loader.ModDirectory, loader.ModArchive)
	}
}

// WithProcessor will add the provided processor to the translator instance. The translator will apply the processors
// in the same order as they were added.
func WithProcessor(p processor.Processor) Option {
	return func(t *Translator) {
		t.processors = append(t.processors, p)
	}
}

// WithCommonProcessors will add common processors to the translator instance, which do not need additional data to be
// applied. These processor mostly process the placeholders, feeding the values back to the translator to continue
// translating the string.
func WithCommonProcessors() Option {
	return func(t *Translator) {
		t.processors = append(
			t.processors,
			processor.EntityPlaceholder,
			processor.ItemPlaceholder,
			processor.PositionPlaceholder,
			processor.PluralPlaceholder,
		)
	}
}

// LoadMod loads the mod stored at source, using the configured loaders. If no loader can load the mod, an error
// is returned.
func (t *Translator) LoadMod(source string) error {
	for _, l := range t.loaders {
		ok := l(t.storage, source)
		if ok {
			return nil
		}
	}

	return fmt.Errorf("factorio translator: no matching loader for mod: %s", source)
}

// ApplyProcessors applies all configured processors to the provided value.
func (t *Translator) ApplyProcessors(locale string, value string, parameters []any) string {
	for _, p := range t.processors {
		value = p(t, locale, value, parameters)
	}
	return value
}

// Translate translates the provided localisedString into the provided locale. If no translation is available, an empty
// string is returned.
func (t *Translator) Translate(locale string, localisedString any) string {
	localisedType := reflect.TypeOf(localisedString)
	if localisedType.Kind() != reflect.Slice {
		return t.ApplyProcessors(locale, fmt.Sprintf("%v", localisedString), make([]any, 0))
	}

	slice := localisedString.([]any)
	if len(slice) == 0 {
		return ""
	}

	if slice[0] == "" {
		return t.concatenate(locale, slice[1:])
	}

	return t.doTranslate(locale, fmt.Sprintf("%v", slice[0]), slice[1:])
}

// TranslateWithFallback is like Translate, but if there is no translation available for the provided locale, it will
// translate it to English as a fallback.
func (t *Translator) TranslateWithFallback(locale string, localisedString any) string {
	result := t.Translate(locale, localisedString)
	if result == "" && locale != fallbackLocale {
		result = t.Translate(fallbackLocale, localisedString)
	}
	return result
}

func (t *Translator) concatenate(locale string, parts []any) string {
	values := make([]string, 0, len(parts))
	for _, part := range parts {
		values = append(values, t.TranslateWithFallback(locale, part))
	}
	return strings.Join(values, "")
}

func (t *Translator) doTranslate(locale string, key string, parameters []any) string {
	var section, name string
	parts := strings.SplitN(key, ".", 2)
	if len(parts) == 1 {
		section = ""
		name = parts[0]
	} else {
		section = parts[0]
		name = parts[1]
	}

	value, ok := t.storage.Get(locale, section, name)
	if !ok {
		return ""
	}

	return t.ApplyProcessors(locale, value, parameters)
}

// Locales returns all locales for which translations are available. You must add any mods before getting any locales.
// It is recommended to at least add the "core" or "base" mod, as they will come with all locales available in the game.
func (t *Translator) Locales() []string {
	return t.storage.Locales()
}
