package translator

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestStorage_SetAndGet(t *testing.T) {
	instance := NewStorage()

	result, ok := instance.Get("abc", "def", "ghi")
	assert.False(t, ok)
	assert.Equal(t, "", result)

	instance.Set("abc", "def", "ghi", "jkl")
	result, ok = instance.Get("abc", "def", "ghi")
	assert.True(t, ok)
	assert.Equal(t, "jkl", result)

	instance.Set("abc", "mno", "ghi", "pqr")
	result, _ = instance.Get("abc", "def", "ghi")
	assert.Equal(t, "jkl", result)
	result, _ = instance.Get("abc", "mno", "ghi")
	assert.Equal(t, "pqr", result)

	instance.Set("abc", "def", "ghi", "stu")
	result, _ = instance.Get("abc", "def", "ghi")
	assert.Equal(t, "stu", result)
}

func TestStorage_Locales(t *testing.T) {
	instance := NewStorage()
	instance.Set("foo", "abc", "def", "ghi")
	instance.Set("foo", "jkl", "mno", "pqr")
	instance.Set("bar", "stu", "vwx", "yza")

	expectedResult := []string{"bar", "foo"}

	result := instance.Locales()

	assert.Equal(t, expectedResult, result)
}
