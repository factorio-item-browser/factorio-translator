package translator

import (
	"fmt"
	"sort"
	"sync"
)

type Storage struct {
	data    map[string]string
	locales map[string]struct{}
	lock    sync.RWMutex
}

// NewStorage creates a storage instance able to hold the translations.
func NewStorage() *Storage {
	return &Storage{
		data:    make(map[string]string),
		locales: make(map[string]struct{}),
	}
}

func (s *Storage) buildKey(locale, section, name string) string {
	return fmt.Sprintf("%s|%s|%s", locale, section, name)
}

func (s *Storage) Set(locale, section, name, value string) {
	s.lock.Lock()
	defer s.lock.Unlock()

	key := s.buildKey(locale, section, name)
	s.data[key] = value
	s.locales[locale] = struct{}{}
}

func (s *Storage) Get(locale, section, name string) (string, bool) {
	s.lock.RLock()
	defer s.lock.RUnlock()

	key := s.buildKey(locale, section, name)
	value, ok := s.data[key]
	return value, ok
}

func (s *Storage) Locales() []string {
	s.lock.RLock()
	defer s.lock.RUnlock()

	locales := make([]string, 0, len(s.locales))
	for locale := range s.locales {
		locales = append(locales, locale)
	}
	sort.Strings(locales)
	return locales
}
