package translator

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/factorio-translator.git/pkg/loader"
	"gitlab.com/factorio-item-browser/factorio-translator.git/pkg/processor"
	"path/filepath"
	"runtime"
	"testing"
)

func TestTranslator_Translate(t *testing.T) {
	_, filename, _, _ := runtime.Caller(0)

	instance := New(
		WithDefaultLoaders(),
		WithLoader(func(loader.Storage, string) bool {
			return false
		}),
		WithCommonProcessors(),
		WithProcessor(processor.NewControlPlaceholderProcessor(func(translator processor.Translator, locale string, controlName string, version int) (string, bool) {
			return fmt.Sprintf("{control=%d %s}", version, controlName), true
		})),
		WithProcessor(processor.NewStandaloneTagProcessor(func(translator processor.Translator, _, name, value string) (string, bool) {
			return fmt.Sprintf("{%s=%s}", name, value), true
		})),
		WithProcessor(processor.NewContentTagProcessor(func(translator processor.Translator, _, name, value, content string) (string, bool) {
			return fmt.Sprintf("{%s=%s}%s{/%s}", name, value, content, name), true
		})),
	)

	err := instance.LoadMod(fmt.Sprintf("%s/../../test/assets/translator", filepath.Dir(filename)))
	assert.Nil(t, err)
	assert.Equal(t, []string{"de", "en", "pl"}, instance.Locales())

	tests := map[string]struct {
		locale          string
		localisedString any
		expectedResult  string
	}{
		// Basic translations
		"basic1": {
			locale:          "en",
			localisedString: []any{"item-name.electronic-circuit"},
			expectedResult:  "Electronic circuit",
		},
		"basic2": {
			locale:          "de",
			localisedString: []any{"item-name.electronic-circuit"},
			expectedResult:  "Elektronischer Schaltkreis",
		},
		// Untranslated strings
		"untranslated": {
			locale:          "en",
			localisedString: "item-name.electronic-circuit",
			expectedResult:  "item-name.electronic-circuit",
		},
		// Recursive translation with English fallback
		"recursive1": {
			locale:          "en",
			localisedString: []any{"recipe-name.fill-barrel", []any{"fluid-name.crude-oil"}},
			expectedResult:  "Fill Crude oil barrel",
		},
		"recursive2": {
			locale:          "de",
			localisedString: []any{"recipe-name.fill-barrel", []any{"fluid-name.crude-oil"}},
			expectedResult:  "Fülle Rohöl in Fass",
		},
		"recursive3": {
			locale:          "en",
			localisedString: []any{"recipe-name.fill-barrel", []any{"fluid-name.lubricant"}},
			expectedResult:  "Fill Lubricant barrel",
		},
		"recursive4": {
			locale:          "de",
			localisedString: []any{"recipe-name.fill-barrel", []any{"fluid-name.lubricant"}},
			expectedResult:  "Fülle Lubricant in Fass",
		},
		// Control placeholders
		"placeholderControl1": {
			locale:          "en",
			localisedString: []any{"item-description.green-wire"},
			expectedResult:  "Used to connect machines to the circuit network using {control=0 build}.",
		},
		"placeholderControl2": {
			locale:          "de",
			localisedString: []any{"item-description.green-wire"},
			expectedResult:  "Verbindet Maschinen mit dem Schaltungsnetz. {control=2 build}, um Verbindungen zu erzeugen oder zu kappen.",
		},
		// Entity placeholders
		"placeholderEntity1": {
			locale:          "en",
			localisedString: []any{"autoplace-control-names.coal"},
			expectedResult:  "Coal",
		},
		"placeholderEntity2": {
			locale:          "de",
			localisedString: []any{"autoplace-control-names.coal"},
			expectedResult:  "Kohle",
		},
		// Item placeholders
		"placeholderItem1": {
			locale:          "en",
			localisedString: []any{"goal-get-resources", 21, 42, 1337, 7331},
			expectedResult:  "Gather resources: \nIron plate: 21/42\nCopper plate: 1337/7331",
		},
		"placeholderItem2": {
			locale:          "de",
			localisedString: []any{"goal-get-resources", 21, 42, 1337, 7331},
			expectedResult:  "Sammle Gegenstände im Inventar:\nEisenplatte: 21/42\nKupferplatte: 1337/7331",
		},
		// Plural forms
		"plural1": {
			locale:          "en",
			localisedString: []any{"hours", 1},
			expectedResult:  "1 hour",
		},
		"plural2": {
			locale:          "en",
			localisedString: []any{"hours", 42},
			expectedResult:  "42 hours",
		},
		"plural3": {
			locale:          "de",
			localisedString: []any{"hours", 1},
			expectedResult:  "1 Stunde",
		},
		"plural4": {
			locale:          "de",
			localisedString: []any{"hours", 42},
			expectedResult:  "42 Stunden",
		},
		"plural5": {
			locale:          "pl",
			localisedString: []any{"hours", 1},
			expectedResult:  "1 godzina",
		},
		"plural6": {
			locale:          "pl",
			localisedString: []any{"hours", 12},
			expectedResult:  "12 godzin",
		},
		"plural7": {
			locale:          "pl",
			localisedString: []any{"hours", 42},
			expectedResult:  "42 godziny",
		},
		"plural8": {
			locale:          "pl",
			localisedString: []any{"hours", 1337},
			expectedResult:  "1337 godzin",
		},
		// Rich text
		"rich1": {
			locale:          "en",
			localisedString: []any{"item-description.empty-dt-fuel"},
			expectedResult:  "Fill it with tritium {item=tritium} and heavy water {fluid=heavy-water} to get a charged cell.",
		},
		"rich2": {
			locale:          "de",
			localisedString: []any{"item-description.empty-dt-fuel"},
			expectedResult:  "Befülle sie mit Tritium {item=tritium} und schwerem Wasser {fluid=heavy-water}, um sie zu laden.",
		},
		"rich3": {
			locale:          "en",
			localisedString: []any{"entity-name.imersite"},
			expectedResult:  "{color=173, 19, 173}Imersite cave{/color}",
		},
		"rich4": {
			locale:          "de",
			localisedString: []any{"entity-name.imersite"},
			expectedResult:  "{color=173, 19, 173}Imersitgrotte{/color}",
		},
		// Concatenating
		"concat1": {
			locale:          "en",
			localisedString: []any{"", []any{"color.blue"}, " ", []any{"tile-name.refined-concrete"}},
			expectedResult:  "Blue Refined concrete",
		},
		"concat2": {
			locale:          "en",
			localisedString: []any{"", "foo ", "[item=iron-plate]", " bar"},
			expectedResult:  "foo {item=iron-plate} bar",
		},
		// Misc
		"empty": {
			locale:          "en",
			localisedString: []any{},
			expectedResult:  "",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			result := instance.Translate(test.locale, test.localisedString)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestTranslator_LoadMod_WithError(t *testing.T) {
	instance := New()
	err := instance.LoadMod("abc")

	assert.NotNil(t, err)
}
