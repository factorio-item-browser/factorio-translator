package main

import (
	"fmt"
	"gitlab.com/factorio-item-browser/factorio-translator.git/pkg/processor"
	"gitlab.com/factorio-item-browser/factorio-translator.git/pkg/translator"
)

func main() {
	// Create instance
	t := translator.New(
		// Use the default loaders for the translator, able to loading zipped and unzipped mods.
		translator.WithDefaultLoaders(),

		// Use the common text processors, handling basic placeholders of the localised strings.
		translator.WithCommonProcessors(),

		// Use an advanced text processor, handling the control placeholders.
		translator.WithProcessor(
			processor.NewControlPlaceholderProcessor(func(translator processor.Translator, locale string, controlName string, version int) (string, bool) {
				// Use the translated name of the control (as it appears in the options menu),
				// and put it into square brackets.
				control := translator.TranslateWithFallback(locale, []any{fmt.Sprintf("controls.%s", controlName)})
				return fmt.Sprintf("[%s]", control), true
			}),
		),
	)

	// Load the mods
	_ = t.LoadMod("/path/to/factorio/data/core")
	_ = t.LoadMod("/path/to/factorio/data/base")
	_ = t.LoadMod("/path/to/factorio/mods/my-fancy-mod_1.33.7.zip")

	// Translate localised strings
	fmt.Println(t.Translate("en", []any{"item-name.electronic-circuit"}))                           // "Electronic circuit"
	fmt.Println(t.Translate("de", []any{"item-name.electronic-circuit"}))                           // "Elektonischer Schaltkreis"
	fmt.Println(t.Translate("en", []any{"recipe-name.fill-barrel", []any{"fluid-name.crude-oil"}})) // "Fill Crude oil barrel""Fill Crude oil barrel"
}
